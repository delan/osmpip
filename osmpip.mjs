import {createInterface} from "readline";
import {writeFile,rename} from "fs/promises";
import {exec} from "child_process";
import {promisify} from "util";
import {stdin,stdout} from "process";
import fetch from "node-fetch";
const asyncExec = promisify(exec);

const TILE_WH = 256;
const TILE_URL = (z,xt,yt) => `http://storage:9892/${z}/${xt}/${yt}.png`;
const TILE_PATH = (z,xt,yt) => `/ocean/private/delan/dashcam/_x/${z}.${xt}.${yt}.png`;
const FRAME_PATH = (i) => `${String(i).padStart(7,0)}.png`;
const FONT_PATH = `/ocean/private/delan/dashcam/mapnik/fonts/dejavu-fonts-ttf-2.37/ttf/DejaVuSans.ttf`;
const DEBUG_FIRST_FRAME = null;
const DEBUG_LAST_FRAME = null;
const ZOOM = 17;
const FPS = 5;
const RENDER_THREADS = 8;

// general utilities
const PI = Math.PI;
const pad0 = (x,w=2) => String(x).padStart(w,0);
const floor = (x) => Math.floor(x);
const log = (x) => Math.log(x);
const tan = (x) => Math.tan(x);
const cos = (x) => Math.cos(x);
const atan = (x) => Math.atan(x);
const exp = (x) => Math.exp(x);
const abs = (x) => Math.abs(x);
const pow = (x,y) => Math.pow(x,y);

// space formats: coordinates and zoom (z)
// • (lat,lon) are in degrees, where negative is south or west
// • (x,y,z) are like slippy map tile offsets, but floating-point in [0,2^z]
// https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
const coord2str = (lat,lon) => `(${lat.toFixed(4)},${lon.toFixed(4)})`;
const lon2x = (lon,z) => (lon+180)/360*pow(2,z);
const lat2y = (lat,z) => (1 - log(tan(lat*PI/180) + 1/cos(lat*PI/180))/PI) / 2 * pow(2,z);
const x2lon = (x,z) => x/pow(2,z)*360 - 180;
const y2lat = (y,z) => { const n = PI - 2*PI*y/pow(2,z); return 180/PI*atan(0.5*(exp(n)-exp(-n))); }
const tile /* get integer part (actual tile name) of fp tile offset */= (xy) => floor(xy);
const phase /* get fractional part of fp tile offset */= (xy) => { return xy-tile(xy)-1.0/* FIXME why 1.0 and not 0.5? */; }
const offset /* crop offset in 3x3 tile group */= (phase) => floor(phase*TILE_WH)+TILE_WH*3/2;

// time formats (t): todRaw + tod + ts
// • input data only contain raw times-of-day (todRaw), so use UTC column to avoid tz effects
// • wraptod creates wrapped times-of-day (tod), where 24:00:00 is midnight the next day of input
// • trip timestamps (ts) are tod minus the first valid tod, so the input data starts at 00:00:00
// • note that we currently make no attempt to correctly handle leap seconds in either direction
const hmsms2str = (h,m,s,ms) => `${hms2str(h,m,s)}.${String(ms).padStart(3,0)}`;
const hms2str = (h,m,s) => `${String(h).padStart(2,0)}:${String(m).padStart(2,0)}:${String(s).padStart(2,0)}`;
const hmsms2t = (h,m,s,ms=0) => h*3600 + m*60 + s + ms/1000;
const t2hmsms = (t) => [floor(t/3600), floor(t/60)%60, floor(t)%60, floor((t-floor(t))*1000)];
const wraptod = (tod,todOld=null/* null if first input line */) => todOld!=null && todOld-tod>=43200 ? tod+86400 : tod;
const delta = (tod,todOld=null/* null if first input line */) => todOld!=null ? tod-todOld : null;

// helpers for promises
const retry /* retry fetch until ok */= async (fetcher) => {
    let result;
    do {
        result = await fetcher();
    } while (!result.ok);
    return result;
};
const makeQueue /* factory for queues that run $threads promises concurrently */= (threads) => {
    const promises = [];
    return async (promiseFactory) => {
        if (promises.length > RENDER_THREADS)
            await Promise.race(promises);
        // https://stackoverflow.com/q/42896456
        const promise = promiseFactory().then(() => {
            promises.splice(promises.indexOf(promise), 1);
        });
        promises.push(promise);
    };
};

const lines = createInterface({
    input: stdin,
    output: null,
    terminal: false
});

(async () => {
    try {
        const data = [];
        let i = 0;
        for await (const line of lines) {
            i++;
            const row = line.split(",").map(x => x.trim());
            const bearing = Number(row[10]);
            if (Number.isNaN(bearing)) continue;
            const [lat,lon] = row.slice(6,8).map(x => Number(x));
            const [h,m,s] = row[5].split(":").map(x => Number(x));

            const todOld = data.at(-1)?.tod;
            const todRaw = hmsms2t(h,m,s);
            const todFirst = data.at(0)?.tod ?? todRaw;
            const tod = wraptod(todRaw,todOld);

            const ts = tod - todFirst;

            const prefix = `[read] line ${i}`;
            stdout.write(`\x1B[K ${prefix}:`
                + ` tod=${tod}(${hms2str(...t2hmsms(tod))})`
                + ` ts=${ts}(${hms2str(...t2hmsms(ts))})`
                + ` ${coord2str(lat,lon)}`
                + `\r`);

            const dt = delta(tod,todOld);
            if (dt != null && dt != 1) {
                console.log();
                console.warn(`\x1B[33m ${prefix}:`
                    + ` bad delta ${dt}`
                    + ` (tod ${hms2str(...t2hmsms(todOld))} -> ${hms2str(...t2hmsms(tod))})...`
                    + ` ${dt > 1 ? `stretching` : `discarding`}`
                    + `\x1B[m`);

                // discard immediately, so data is strictly increasing
                const discard = dt < 1;
                if (discard) continue;
            }

            const stretch = dt != null && dt > 1;
            data.push({tod, lat, lon, bearing, stretch});
        }

        // don’t clobber progress line with further output
        console.log();

        const renderEnqueue = makeQueue(RENDER_THREADS);
        const renderStart = performance.now();
        const todStart = data.at(0).tod;
        const duration = data.at(-1).tod - todStart;

        const first = DEBUG_FIRST_FRAME ?? 0;
        const last = DEBUG_LAST_FRAME ?? duration * FPS;

        for (let i = first; i <= last; i++) {
            const ts = i / FPS;

            const next = data.findIndex(x => x.tod > todStart+ts);
            const lerp = next!=-1;
            const i0 = lerp ? next-1 : data.length-1;
            const i1 = lerp ? next : null;

            const tod0 = data[i0].tod;
            const tod1 = data[i1]?.tod;
            const mix = lerp ? (ts-(tod0-todStart))/(tod1-tod0) : null;

            const prefix = `[render] frame ${i}/${last}`;
            const renderFps = (i - first) / ((performance.now() - renderStart) / 1000);
            const renderEta = (last - i + 1) / renderFps;
            stdout.write(`\x1B[K ${prefix}:`
                + ` ts=${hmsms2str(...t2hmsms(ts))}`
                + ` tod=${hms2str(...t2hmsms(tod0))}+${tod1-tod0}`
                + ` fps=${renderFps.toFixed(2)}`
                + ` eta=${renderEta.toFixed(2)}`
                + `\r`);

            const lat = !lerp ? data[i0].lat : (1-mix)*data[i0].lat + mix*data[i1].lat;
            const lon = !lerp ? data[i0].lon : (1-mix)*data[i0].lon + mix*data[i1].lon;
            // always lerp bearing in shorter direction (cw or ccw), even if crossing 0° boundary
            const bearing = !lerp ? data[i0].bearing : (1-mix)*data[i0].bearing + mix*(data[i1].bearing+(
                abs(data[i0].bearing-data[i1].bearing) >= 180 ? 360 : 0));
            const x = lon2x(lon,ZOOM);
            const y = lat2y(lat,ZOOM);
            const xt = tile(x);
            const yt = tile(y);
            const xoff = offset(phase(x));
            const yoff = offset(phase(y));

            const tiles = [
                [xt-1,yt-1], [xt,yt-1], [xt+1,yt-1],
                [xt-1,yt], [xt,yt], [xt+1,yt],
                [xt-1,yt+1], [xt,yt+1], [xt+1,yt+1],
            ];
            const urls = tiles.map(([xt,yt]) => TILE_URL(ZOOM,xt,yt));
            const promises = urls.map(url => retry(() => fetch(url)));
            const results = await Promise.all(promises);
            const paths = tiles.map(([xt,yt]) => TILE_PATH(ZOOM,xt,yt));
            for (const [i, result] of results.entries()) {
                const buffer = await result.buffer();
                await writeFile(`${paths[i]}.incomplete`, buffer);
                await rename(`${paths[i]}.incomplete`, paths[i]);
            }

            const path = FRAME_PATH(i);

            // convert coordinates from top-left-of-square-pixels to centre-of-samples
            // imagemagick calls these “image coordinates” and “pixel coordinates” respectively
            // https://legacy.imagemagick.org/Usage/draw/#coordinates
            const transform = `-affine 1,0,0,1,-0.5,-0.5`;
            const shapeTransform = `translate ${TILE_WH/2},${TILE_WH/2} rotate ${bearing}`;
            // circles (x0,y0) (x1,y1) are centre and radius, not corners of AABB
            const shape = data[i1]?.stretch ? `circle 0,0 4,4` : `polygon 0,-6 -4,6 4,6`;
            const scMarker = data[i1]?.stretch ? `#66339980` : `#663399`;
            const fcMarker = data[i1]?.stretch ? `#ffffff80` : `#ffffff`;
            const scText = `#663399`;
            const fcText = `#fff`;
            // do not remove this attribution
            // https://wiki.osmfoundation.org/wiki/Licence/Licence_and_Legal_FAQ#How_should_I_attribute_you.3F
            const text = `© OpenStreetMap`;

            const montage = `montage ${paths.join(" ")} -tile 3x3 -geometry +0+0 png:-`;
            const font = `-font ${FONT_PATH} -pointsize 10 -strokewidth 4`;
            const crop = `-crop ${TILE_WH}x${TILE_WH}+${xoff}+${yoff} +repage`;
            const stroke = `-stroke '${scMarker}' -draw 'fill ${scMarker} ${shapeTransform} ${shape}'`;
            const fill = `-stroke '#0000' -draw 'fill ${fcMarker} ${shapeTransform} ${shape}'`;
            const shadow = `-stroke '${scText}' -draw 'fill ${scText} gravity SouthEast text 4,4 "${text}"'`;
            const proper = `-stroke '#0000' -draw 'fill ${fcText} gravity SouthEast text 4,4 "${text}"'`;
            const render = `convert png:- ${font} ${crop} ${transform} ${stroke} ${fill} ${shadow} ${proper} ${path}.incomplete`;
            const command = `${montage} | ${render}`;
            await renderEnqueue(async () => {
                await asyncExec(command);
                await rename(`${path}.incomplete`, path);
            });
        }

        // don’t clobber progress line with output after process exits
        console.log();
    } catch (e) {
        // don’t clobber progress lines with uncaught exception output
        console.log();
        throw e;
    }
})();
