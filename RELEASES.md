# 0.0.7 (2021-10-20)


* add a README, release notes, example input, and package{,-lock}.json
* polish code and explain a bunch of tricky parts


# 0.0.6 (2021-10-19)


* fix bug where bearing would fail to lerp across 0° boundary
* fix bug where we accepted backwards deltas of [0.5,12) hours
* fix bug where uncaught exceptions would clobber progress lines
* write frames to current directory by default (FRAME_PATH)
* render 5 fps by default (FPS)


# 0.0.5 (2021-10-17)


* fix bug where ~80 lines of input were skipped (was for debugging)
* calculate and show both times-of-day and timestamps in all output
* refactor code to rigorously separate times-of-day and timestamps
* add constant for maximum imagemagick concurrency (RENDER_THREADS)
* clean up other global constants (z, fps, and the long-unused lon)


# 0.0.4 (2021-10-17)


* lerp bearing angles too, in addition to (lat,lon)
* use a circular marker when stretching lerp over missing data
* fix bug where marker was off-centre (coordinate space mismatch)
* thicken strokes of marker and text shadow to improve legibility
* allow rendering only a subset of frames (DEBUG_{FIRST,LAST},FRAME)


# 0.0.3 (2021-10-17)


* enlarge marker and separate fill/stroke to make it more legible
* build imagemagick command in logical steps for readability
* burn “© OpenStreetMap” into frames to comply with OSM licence


# 0.0.2 (2021-10-16)


* improve CPU utilisation by doing imagemagick work asynchronously
* write tiles atomically (path.incomplete) to avoid corruption
* replace dot marker with a triangle that rotates with bearing data
* fade marker (alpha = 80h) if lerping over longer than one second
* calculate and show fps + eta in render progress
* extract frame output path to a global constant (FRAME_PATH)


# 0.0.1 (2021-10-16)


* decouple reading and rendering, lerping over (lat,lon)
* render at a fixed 4 fps, rather than one frame per row
* discard input with non-strictly-increasing times-of-day
* prettier read/render progress lines that update in place


# 0.0.0 (2021-10-16)


* initial release
