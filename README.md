osmpip
======


Reads dashcam GPS data from standard input, then renders an OpenStreetMap-based animation of your trip.


Requirements
------------


* Node.js 16.6+ (16.10.0 tested)
* POSIX /bin/sh (FreeBSD 13 tested)
* ImageMagick (7.0.11 tested)
* OpenStreetMap tile server


**Do not use this script with tile servers you don’t own.** This includes the main instance at tile.openstreetmap.org, even though its [Tile Usage Policy](https://operations.osmfoundation.org/policies/tiles/) allows (but strongly discourages) bulk downloads in limited circumstances. Even if this script had perfect local caching (and it doesn’t), it’s safe to assume that no operator of a public tile server would be happy with our usage.


Usage
-----


1. Set up your own tile server: <https://switch2osm.org/serving-tiles/>
2. Configure the GLOBAL_CONSTANTS near the top of osmpip.mjs
3. Install dependencies (once) and run osmpip as follows:


```
$ npm i
$ cd path/to/output/directory
$ node path/to/osmpip < path/to/osmpip/example.csv
```


To convert the resultant frames to a video, you can use ffmpeg:


```
$ ffmpeg -pattern_type glob -r $fps -i \*.png -c:v libx264 -crf 0 path/to/out.mp4
```


Input format
------------


Data should be in the [Dashcam Viewer](https://dashcamviewer.com) CSV dump format:


```
unused, unused, unused, unused, unused, time, lat, lon, unused, unused, bearing,
```


* **time** is a time of day (HH:MM:SS), ideally in UTC
* **lat** is in degrees (±n.nn...), where negative is south
* **lon** is in degrees (±n.nn...), where negative is west
* **bearing** is in degrees (n.nn...), where 0° is north
